from distutils.core import setup


setup(
    name='formosa',
    version='0.3.3',
    author='Hao-Yung Chan',
    author_email='katrina.hyc@gmail.com',
    packages=[
		'formosa', 
		'formosa.models', 
		'formosa.utils', 
	],
	package_data={
		'formosa': ['assets/*.css'],
	},
    license='LICENSE',
    description='Generate and color SVG Taiwan maps.',
    long_description=open('README.md').read(),
    install_requires=[
        'svgwrite',
        'pyshp',
    ],
)
