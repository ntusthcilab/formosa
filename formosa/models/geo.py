from xml.etree import ElementTree as et

import shapefile


NS = {
    'pub': 'http://standards.moi.gov.tw/schema/pub',
    'gml': 'http://www.opengis.net/gml',
}


class ProcessFileError(Exception):
    def __init__(self, fp):
        message = f'Cannot process file as GML or SHP: {fp}'
        super(Exception, self).__init__(message)


class BoundaryIterator:
    def __init__(self, core):
        self._core = core
        self._index = 0

    def __next__(self):
        if self._index < len(self._core.points):
           self._index += 1
           return self._core.points[self._index - 1]
        raise StopIteration # end


class Boundary:
    @classmethod
    def from_string(cls, line):
        points = tuple([
            tuple(float(s) for s in coor.split(','))
            for coor in line.split(' ')
        ])
        return cls(points)

    def __init__(self, points):
        self.points = points
    
    def __iter__(self):
        return BoundaryIterator(self)

    def __hash__(self):
        return hash(self.points)


class District:
    @classmethod
    def from_file(cls, fp):
        try:
            tree = et.parse(fp)
            return cls.from_gml(fp)
        except et.ParseError:
            pass

        try:
            with shapefile.Reader(fp):
                return cls.from_shp(fp)
        except:
            raise ProcessFileError(fp)

    @classmethod
    def from_gml(cls, fp):
        tree = et.parse(fp)
        root = tree.getroot()
        nodes = root.findall('gml:featureMember', NS)

        for node in nodes:
            region = node.find('pub:PUB_行政區域', NS)

            code = region.find('pub:行政區域代碼', NS).text
            name = region.find('pub:名稱', NS).text

            area = region.find('pub:涵蓋範圍', NS)
            polygons = area.findall('gml:MultiPolygon/gml:polygonMember/gml:Polygon', NS)

            members = []
            for m in polygons:
                for n in m:
                    members.append(n.find('gml:LinearRing/gml:coordinates', NS).text)

            boundaries = [
                Boundary.from_string(line) for line in members
            ]

            yield cls(code, name, boundaries)

    @classmethod
    def from_shp(cls, fp, parser=None):
        with shapefile.Reader(fp) as sf:
            srs = sf.shapeRecords()

        for s in srs:
            if parser is not None:
                code, name = parser(s.record)
            elif hasattr(s.record, 'TOWNCODE'):
                code = s.record.TOWNCODE
                name = f'{s.record.COUNTYNAME}{s.record.TOWNNAME}'
            else:
                code = s.record.COUNTYCODE
                name = s.record.COUNTYNAME

            boundaries = []

            for idx, start in enumerate(s.shape.parts):
                end = s.shape.parts[idx + 1] if idx + 1 < len(s.shape.parts) else None
                boundaries.append(Boundary(tuple(s.shape.points[start:end])))

            yield cls(code, name, boundaries)

    def __init__(self, code, name, boundaries):
        self.code = code
        self.name = name
        self.boundaries = boundaries
