class Box:
    dwg = None

    def __init__(self, name, position, size):
        if self.dwg is None:
            raise ValueError('`dwg` should be set.')

        if not isinstance(name, str):
            raise TypeError('`name` should be a string.')

        if not isinstance(position, tuple) or len(position) != 2:
            raise TypeError('`position` should be a tuple with the length of 2.')

        if not isinstance(size, tuple) or len(size) != 2:
            raise TypeError('`size` should be a tuple with the length of 2.')

        self.name = name

        self.size = (
            size[0] * self.dwg['width'],
            size[1] * self.dwg['height'],
        )
        self.position = (
            position[0] * self.dwg['width'],
            position[1] * self.dwg['height'],
        )

        self.g = self.dwg.g(
            id='group-' + name,
        )
        self.g.translate(*self.position)
        self.g.add(
            self.dwg.rect(
                size=self.size,
                id='rect-' + name,
                class_='mapbox-base'
            )
        )

        self.dwg.add(self.g)


class MapBox(Box):
    def __init__(self, name, position, size, border, skip, display_name):
        super(MapBox, self).__init__(name, position, size)

        if not isinstance(display_name, str):
            raise TypeError('`display_name` should be a string.')

        if not isinstance(position, tuple) or len(position) != 2:
            raise TypeError('`position` should be a tuple with the length of 2.')

        if not isinstance(size, tuple) or len(size) != 2:
            raise TypeError('`size` should be a tuple with the length of 2.')

        self.display_name = display_name

        self.border = border
        self.skip = skip

        self.clip = self.dwg.defs.add(
            self.dwg.clipPath(
                id='clip-' + name
            )
        )
        self.clip.add(self.dwg.rect(size=self.size))

        padding = self.dwg['width'] * 0.02
        self.g.add(
            self.dwg.text(
                self.display_name,
                insert=(self.size[0] - padding, self.size[1] - padding),
                text_anchor='end',
                class_='mapbox-name',
            )
        )

    def add_polygon(self, code, coordinates, kind):
        points = self._remap([
            self._scale(*p)
            for idx, p in enumerate(coordinates)
        ])
        self.g.add(
            self.dwg.path(
                d=points,
                code=code,
                class_=kind,
                clip_path='url(#clip-{})'.format(self.name)
            )
        )

    def add_district(self, district, kind):
        paths = [
            self._remap([self._scale(x, y) for x, y in boundary])
            for boundary in district.boundaries
        ]
        self.g.add(
            self.dwg.path(
                d=' '.join(paths),
                code=district.code,
                class_=kind,
                clip_path='url(#clip-{})'.format(self.name)
            )
        )

    def _scale(self, x, y):
        xmin, xmax, ymin, ymax = self.border
        width, height = self.size

        s = min(width / (xmax - xmin), height / (ymax - ymin))

        nx = (x - xmin) * s
        ny = height + (- y + ymin) * s

        return nx, ny

    def _remap(self, points):
        fx, fy = points[0]
        npoints = [(round(fx), round(fy))]

        for idx in range(1, len(points)):
            if idx % self.skip != 0:
                continue
            px, py = npoints[-1]
            x, y = points[idx]
            if round(px) != round(x) or round(py) != round(y):
                npoints.append((round(x), round(y)))

        serial = ' '.join([f'{p[0]},{p[1]}' for p in npoints])
        ret = f'M{serial}z'

        return ret
