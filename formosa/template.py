import svgwrite

import os.path

from .models.plot import Box, MapBox
from .models.geo import District
from .meta import STYLEPATH, GROUPS, ASSIGN_RULES


def create_from_obj(output, config):
    if not hasattr(config, 'AREA_FILE_PATH'):
        raise ValueError('AREA_FILE_PATH is not set.')
    else:
        area = config.AREA_FILE_PATH

    border = getattr(config, 'BORDER_FILE_PATH', area)
    size = getattr(config, 'SIZE', (2000, 2000))

    options = {
        'stylesheet': getattr(config, 'STYLEPATH', STYLEPATH),
        'groups': getattr(config, 'GROUPS', GROUPS),
        'assign_rules': getattr(config, 'ASSIGN_RULES', ASSIGN_RULES),
    }

    return create_from_file(output, area, border, size, **options)


def create_from_file(output, area, border=None, size=(2000, 2000), **options):
    if border is None:
        border = area

    if not os.path.isfile(area):
        raise FileNotFoundError(f'Area path is not a regular file: {area}')

    if not os.path.isfile(border):
        raise FileNotFoundError(f'Border path is not a regular file: {border}')

    area_districts = District.from_file(area)
    border_districts = District.from_file(border)

    return create(output, area_districts, border_districts, size, **options)


def create(output, area_districts, border_districts, size=(2000, 2000), **options):
    assign_rules = options.get('assign_rules', ASSIGN_RULES)
    groups = options.get('groups', GROUPS)

    for func, key in assign_rules:
        if key not in groups:
            raise KeyError(f'Assign rule is missing from the groups: {key}')

    if 'main' not in groups:
        raise KeyError('`main` is required in groups')

    for key, g in groups.items():
        if not isinstance(g, dict):
            raise TypeError(f'`Group should be a dict: {key}')

        for field in ('display_name', 'position', 'size', 'border', 'skip'):
            if field not in g:
                raise KeyError(f'`{field}` is missing from group: {key}')

    dwg = svgwrite.Drawing(output, size=size, profile='tiny', debug=False)

    stylesheet = options.get('stylesheet', STYLEPATH)

    with open(stylesheet, 'r') as f:
        dwg.add(dwg.style(f.read() + '#__extension_anchor {}'))

    Box.dwg = dwg
    # use static variable for singleton

    boxes = {
        name: MapBox(
            name,
            **{k:v for k, v in group.items()})
        for name, group in groups.items()
    }

    area_coords = {}

    for dst in area_districts:
        box_name = apply_assign_rule(dst, assign_rules)
        boxes.get(box_name).add_district(dst, 'area')
        area_coords.update({b.points: box_name for b in dst.boundaries})

    for dst in border_districts:
        box_name = apply_assign_rule(dst, assign_rules)
        for b in dst.boundaries:
            reassign_box_name = area_coords.get(b.points, box_name)
            boxes.get(reassign_box_name).add_polygon(dst.code, b.points, 'border')

    dwg.save()


def apply_assign_rule(elm, rules):
    matches = [key for func, key in rules if func(elm.name)] + ['main']
    return matches[0]
